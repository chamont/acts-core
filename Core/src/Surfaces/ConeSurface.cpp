// This file is part of the ACTS project.
//
// Copyright (C) 2016 ACTS project team
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

///////////////////////////////////////////////////////////////////
// ConeSurface.cpp, ACTS project
///////////////////////////////////////////////////////////////////

#include "ACTS/Surfaces/ConeSurface.hpp"

#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>

#include "ACTS/Utilities/ThrowAssert.hpp"
#include "ACTS/Utilities/detail/RealQuadraticEquation.hpp"

Acts::ConeSurface::ConeSurface(const ConeSurface& other)
  : GeometryObject(), Surface(other), m_bounds(other.m_bounds)
{
}

Acts::ConeSurface::ConeSurface(const ConeSurface& other,
                               const Transform3D& transf)
  : GeometryObject(), Surface(other, transf), m_bounds(other.m_bounds)
{
}

Acts::ConeSurface::ConeSurface(std::shared_ptr<const Transform3D> htrans,
                               double                             alpha,
                               bool                               symmetric)
  : GeometryObject()
  , Surface(htrans)
  , m_bounds(std::make_shared<const ConeBounds>(alpha, symmetric))
{
}

Acts::ConeSurface::ConeSurface(std::shared_ptr<const Transform3D> htrans,
                               double                             alpha,
                               double                             zmin,
                               double                             zmax,
                               double                             halfPhi)
  : GeometryObject()
  , Surface(htrans)
  , m_bounds(std::make_shared<const ConeBounds>(alpha, zmin, zmax, halfPhi))
{
}

Acts::ConeSurface::ConeSurface(std::shared_ptr<const Transform3D> htrans,
                               std::shared_ptr<const ConeBounds>  cbounds)
  : GeometryObject(), Surface(htrans), m_bounds(cbounds)
{
  throw_assert(cbounds, "ConeBounds must not be nullptr");
}

Acts::ConeSurface::~ConeSurface()
{
}

const Acts::Vector3D
Acts::ConeSurface::binningPosition(Acts::BinningValue bValue) const
{
  // special binning type for R-type methods
  if (bValue == Acts::binR || bValue == Acts::binRPhi)
    return Vector3D(
        center().x() + bounds().r(center().z()), center().y(), center().z());
  // give the center as default for all of these binning types
  // binX, binY, binZ, binR, binPhi, binRPhi, binH, binEta
  return center();
}

Acts::Surface::SurfaceType
Acts::ConeSurface::type() const
{
  return Surface::Cone;
}

Acts::ConeSurface&
Acts::ConeSurface::operator=(const ConeSurface& other)
{
  if (this != &other) {
    Surface::operator=(other);
    m_bounds         = other.m_bounds;
  }
  return *this;
}

const Acts::Vector3D
Acts::ConeSurface::rotSymmetryAxis() const
{
  return std::move(transform().matrix().block<3, 1>(0, 2));
}

const Acts::RotationMatrix3D
Acts::ConeSurface::referenceFrame(const Vector3D& pos, const Vector3D&) const
{
  RotationMatrix3D mFrame;
  // construct the measurement frame
  // measured Y is the local z axis
  Vector3D measY = rotSymmetryAxis();
  // measured z is the position transverse normalized
  Vector3D measDepth = Vector3D(pos.x(), pos.y(), 0.).unit();
  // measured X is what comoes out of it
  Acts::Vector3D measX(measY.cross(measDepth).unit());
  // the columnes
  mFrame.col(0) = measX;
  mFrame.col(1) = measY;
  mFrame.col(2) = measDepth;
  // return the rotation matrix
  //!< @todo fold in alpha
  // return it
  return mFrame;
}

void
Acts::ConeSurface::localToGlobal(const Vector2D& lpos,
                                 const Vector3D&,
                                 Vector3D& gpos) const
{
  // create the position in the local 3d frame
  double   r   = lpos[Acts::eLOC_Z] * bounds().tanAlpha();
  double   phi = lpos[Acts::eLOC_RPHI] / r;
  Vector3D loc3Dframe(r * cos(phi), r * sin(phi), lpos[Acts::eLOC_Z]);
  // transport it to the globalframe
  if (m_transform) gpos = transform() * loc3Dframe;
}

bool
Acts::ConeSurface::globalToLocal(const Vector3D& gpos,
                                 const Vector3D&,
                                 Vector2D& lpos) const
{
  Vector3D loc3Dframe = m_transform ? (transform().inverse() * gpos) : gpos;
  double   r          = loc3Dframe.z() * bounds().tanAlpha();
  lpos = Vector2D(r * atan2(loc3Dframe.y(), loc3Dframe.x()), loc3Dframe.z());
  // now decide on the quility of the transformation
  double inttol = r * 0.0001;
  inttol        = (inttol < 0.01) ? 0.01 : 0.01;  // ?
  return ((std::abs(loc3Dframe.perp() - r) > inttol) ? false : true);
}

Acts::Intersection
Acts::ConeSurface::intersectionEstimate(const Vector3D&      gpos,
                                        const Vector3D&      gdir,
                                        bool                 forceDir,
                                        const BoundaryCheck& bcheck) const
{
  // transform to a frame with the cone along z, with the tip at 0
  Vector3D tpos1 = m_transform ? transform().inverse() * gpos : gpos;
  Vector3D tdir  = m_transform ? transform().inverse().linear() * gdir : gdir;
  // see the header for the formula derivation
  double tan2Alpha = bounds().tanAlpha() * bounds().tanAlpha(),
         A         = tdir.x() * tdir.x() + tdir.y() * tdir.y()
      - tan2Alpha * tdir.z() * tdir.z(),
         B = 2 * (tdir.x() * tpos1.x() + tdir.y() * tpos1.y()
                  - tan2Alpha * tdir.z() * tpos1.z()),
         C = tpos1.x() * tpos1.x() + tpos1.y() * tpos1.y()
      - tan2Alpha * tpos1.z() * tpos1.z();
  if (A == 0.) A += 1e-16;  // avoid div by zero

  // use Andreas' quad solver, much more stable than what I wrote
  detail::RealQuadraticEquation solns(A, B, C);

  Vector3D solution(0., 0., 0.);
  double   path    = 0.;
  bool     isValid = false;
  if (solns.solutions != 0) {
    double         t1 = solns.first;
    Acts::Vector3D soln1Loc(tpos1 + t1 * tdir);
    isValid = forceDir ? (t1 > 0.) : true;
    // there's only one solution
    if (solns.solutions == 1) {
      solution = soln1Loc;
      path     = t1;
    } else {
      double   t2 = solns.second;
      Vector3D soln2Loc(tpos1 + t2 * tdir);
      // both solutions have the same sign
      if (t1 * t2 > 0. || !forceDir) {
        if (t1 * t1 < t2 * t2) {
          solution = soln1Loc;
          path     = t1;
        } else {
          solution = soln2Loc;
          path     = t2;
        }
      } else {
        if (t1 > 0.) {
          solution = soln1Loc;
          path     = t1;
        } else {
          solution = soln2Loc;
          path     = t2;
        }
      }
    }
  }
  if (m_transform) solution = transform() * solution;

  isValid = bcheck ? (isValid && isOnSurface(solution, bcheck)) : isValid;
  return Intersection(solution, path, isValid);
}

double
Acts::ConeSurface::pathCorrection(const Vector3D& gpos,
                                  const Vector3D& mom) const
{
  // (cos phi cos alpha, sin phi cos alpha, sgn z sin alpha)
  Vector3D posLocal = m_transform ? transform().inverse() * gpos : gpos;
  double   phi      = posLocal.phi();
  double   sgn      = posLocal.z() > 0. ? -1. : +1.;
  Vector3D normalC(cos(phi) * bounds().cosAlpha(),
                   sin(phi) * bounds().cosAlpha(),
                   sgn * bounds().sinAlpha());
  if (m_transform) normalC = transform() * normalC;
  // back in global frame
  double cAlpha = normalC.dot(mom.unit());
  return std::abs(1. / cAlpha);
}

std::string
Acts::ConeSurface::name() const
{
  return "Acts::ConeSurface";
}

Acts::ConeSurface*
Acts::ConeSurface::clone(const Acts::Transform3D* shift) const
{
  if (shift) new ConeSurface(*this, *shift);
  return new ConeSurface(*this);
}

const Acts::Vector3D
Acts::ConeSurface::normal(const Acts::Vector2D& lp) const
{
  // (cos phi cos alpha, sin phi cos alpha, sgn z sin alpha)
  double phi = lp[Acts::eLOC_RPHI] / (bounds().r(lp[Acts::eLOC_Z])),
         sgn = lp[Acts::eLOC_Z] > 0 ? -1. : +1.;
  Vector3D localNormal(cos(phi) * bounds().cosAlpha(),
                       sin(phi) * bounds().cosAlpha(),
                       sgn * bounds().sinAlpha());
  return m_transform ? Vector3D(transform().linear() * localNormal)
                     : localNormal;
}

const Acts::Vector3D
Acts::ConeSurface::normal(const Acts::Vector3D& gpos) const
{
  // get it into the cylinder frame if needed
  // @todo respect opening angle
  Vector3D pos3D = gpos;
  if (m_transform || m_associatedDetElement) {
    pos3D     = transform().inverse() * gpos;
    pos3D.z() = 0;
  }
  return pos3D.unit();
}

const Acts::ConeBounds&
Acts::ConeSurface::bounds() const
{
  // is safe because no constructor w/o bounds exists
  return (*m_bounds.get());
}
